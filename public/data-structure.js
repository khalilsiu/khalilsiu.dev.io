

// CPU is exp, the fastest, runs calculations
// CPU and RAM storage, will disappear when no battery, temp storage
// SSD and HDD are persistent storage
// memory is like addresses, array is a contiguous memory, easy to find with index

// Array list
const arr = [];

// Array list is slow in addition / insertion
// Even js list > memory would reallocate new length of memory and copy those element there
// Linked list is faster with insertion, deletion


// Object == dictionary == hash table == map

// set > only stores unique values by checking with the hash function

let birthdays = new Set();

birthdays.add("1990-01-01");
birthdays.add("1990-01-02");
birthdays.add("1990-01-03");
birthdays.add("1990-01-01");

console.log(birthdays);

console.log(birthdays.has('1990-01-01'));

let arrofdiffnum = [1,1,1,2,2,3,3,4]

let uniquenum = new Set(arrofdiffnum);

// this gives a {}

console.log([...uniquenum]);
// or, to return back list
console.log(Array.from(uniquenum));


// the call stack is really a stack structure
// js array uses push and pop >> LIFO
// queue is really like a queue, FIFO, js uses push and shift
// indeed stack and queue are the same, just 

// binary tree, every node is larger than left, smaller than right
// but traversing will get longer and longer as elements are added
// AVL tree checks if binary tree is balanced left right after inserting

const arr = [6,4,3,35,5,2,2,1,5,6,7,3,343];

// this is going to sort them like string, not what we want
arr.sort();

// we need to define a function ourselves defining the order
// > 0 first comes first

arr.sort(function(first, second){
    return first - second;
});

console.log(arr);

// quicksort make use of recursive function
// get array of larger than pivot, array smaller than pivot, rerun the quicksort function within the larger array and smaller array
// until reaching the base case > array.length < 2
// js sort is using quicksort


//searching
// spits the element that returns true
console.log([4,2,2,3,4,6,34,54,8,4].find(function(value){
    return value === 54;
}));

// if you need to search for stuff all the time, do not use linear search, slow

// copyleft >> I am open sourced, if you use my program, you have to be open sourced 

// free != open source e.g. dropbox is free, but they dont give you the source code (closed)